==============================
API for linkage disequilibrium
==============================

*******************
The LDstats object
*******************

.. autoclass:: moments.LD.LDstats
    :members:

*********************
Demographic functions
*********************

.. automodule:: moments.LD.Demographics1D
    :members:

.. automodule:: moments.LD.Demographics2D
    :members:

.. automodule:: moments.LD.Demographics3D
    :members:

*******************
Inference functions
*******************

.. note::
    These docstrings need attention!

.. automodule:: moments.LD.Inference
    :members:

**************
Util functions
**************

.. automodule:: moments.LD.Util
    :members:

*****************
Parsing functions
*****************

.. automodule:: moments.LD.Parsing
    :members:
